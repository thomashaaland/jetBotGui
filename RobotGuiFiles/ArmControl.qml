import QtQuick 2.15
import QtQuick.Controls
import QtQuick.Layouts

Item {
    implicitWidth: cl.implicitWidth
    implicitHeight: cl.implicitHeight
    property string ipAddress: ""
    property string stopRequest: "/stop"
    ColumnLayout {
        id: cl
        implicitWidth: children.width
        implicitHeight: children.height
        RowLayout {
            width: children.width
            height: children.height
            Layout.alignment: Qt.AlignHCenter
            Button {
                Layout.preferredWidth: 120
                property string request: "/clawopen"
                Rectangle {
                    width: parent.width/1.05
                    height: parent.height/1.1
                    radius: 5
                    color: "red"
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: "CLAW OPEN"
                    }
                }
                onPressed: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + request;
                    Http.open("GET", url);
                    Http.send();
                }
            }
            Button {
                Layout.preferredWidth: 120
                property string request: "/clawclose"
                Rectangle {
                    width: parent.width/1.05
                    height: parent.height/1.1
                    radius: 5
                    color: "red"
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: "CLAW CLOSE"
                    }
                }                    onPressed: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + request;
                    Http.open("GET", url);
                    Http.send();
                }
            }
        }
        RowLayout {
            width: children.width
            height: children.height
            Layout.alignment: Qt.AlignHCenter
            Button {
                Layout.preferredWidth: 120
                property string request: "/camup"
                Rectangle {
                    width: parent.width/1.05
                    height: parent.height/1.1
                    radius: 5
                    color: "red"
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: "CAM UP"
                    }
                }
                onPressed: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + request;
                    Http.open("GET", url);
                    Http.send();
                }
            }
            Button {
                Layout.preferredWidth: 120
                property string request: "/yIn"
                Rectangle {
                    width: parent.width/1.05
                    height: parent.height/1.1
                    radius: 5
                    color: "red"
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: "CLAW UP"
                    }
                }                    onPressed: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + request;
                    Http.open("GET", url);
                    Http.send();
                }
            }
        }
        RowLayout {
            width: children.width
            height: children.height
            Button {
                Layout.preferredWidth: 120
                property string request: "/armleft"
                Rectangle {
                    width: parent.width/1.05
                    height: parent.height/1.1
                    radius: 5
                    color: "red"
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: "PAN LEFT"
                    }
                }                    onPressed: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + request;
                    Http.open("GET", url);
                    Http.send();
                }
            }
            Button {
                Layout.preferredWidth: 120
                property string request: "/camdown"
                Rectangle {
                    width: parent.width/1.05
                    height: parent.height/1.1
                    radius: 5
                    color: "red"
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: "CAM DOWN"
                    }
                }                    onPressed: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + request;
                    Http.open("GET", url);
                    Http.send();
                }
            }
            Button {
                Layout.preferredWidth: 120
                property string request: "/yDe"
                Rectangle {
                    width: parent.width/1.05
                    height: parent.height/1.1
                    radius: 5
                    color: "red"
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: "CLAW DOWN"
                    }
                }                    onPressed: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + request;
                    Http.open("GET", url);
                    Http.send();
                }
            }
            Button {
                Layout.preferredWidth: 120
                property string request: "/armright"
                Rectangle {
                    width: parent.width/1.05
                    height: parent.height/1.1
                    radius: 5
                    color: "red"
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: "PAN RIGHT"
                    }
                }                    onPressed: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + request;
                    Http.open("GET", url);
                    Http.send();
                }
            }
        }
        RowLayout {
            width: children.width
            height: children.height
            Layout.alignment: Qt.AlignHCenter
            Button {
                Layout.preferredWidth: 120
                property string request: "/xIn"
                Rectangle {
                    width: parent.width/1.05
                    height: parent.height/1.1
                    radius: 5
                    color: "red"
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: "CLAW FORWARD"
                    }
                }
                onPressed: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + request;
                    Http.open("GET", url);
                    Http.send();
                }
            }
            Button {
                Layout.preferredWidth: 120
                property string request: "/xDe"
                Rectangle {
                    width: parent.width/1.05
                    height: parent.height/1.1
                    radius: 5
                    color: "red"
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: "CLAW BACK"
                    }
                }
                onPressed: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + request;
                    Http.open("GET", url);
                    Http.send();
                }
            }
        }
    }
}
