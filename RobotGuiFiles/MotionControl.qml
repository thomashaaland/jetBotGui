import QtQuick 2.15
import QtQuick.Controls
import QtQuick.Layouts

Item {
    implicitWidth: cl.implicitWidth
    implicitHeight: cl.implicitHeight
    property string ipAddress: ""
    property string stopRequest: "/stop"
    ColumnLayout {
        id: cl
        implicitWidth: children.width
        implicitHeight: children.height
        Button {
            Layout.preferredWidth: 120
            property string request: "/forward"
            Layout.alignment: Qt.AlignHCenter
            Rectangle {
                width: parent.width/1.05
                height: parent.height/1.1
                radius: 5
                color: "red"
                Label {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "FORWARD"
                }
            }
            onPressed: function() {
                const Http = new XMLHttpRequest();
                const url = ipAddress + request;
                Http.open("GET", url);
                Http.send();
            }
            onReleased: function() {
                const Http = new XMLHttpRequest();
                const url = ipAddress + stopRequest;
                Http.open("GET", url);
                Http.send();
            }
        }
        RowLayout {
            width: children.width
            height: children.height
            Button {
                Layout.preferredWidth: 120
                property string request: "/left"
                Rectangle {
                    width: parent.width/1.05
                    height: parent.height/1.1
                    radius: 5
                    color: "red"
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: "LEFT"
                    }
                }                    onPressed: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + request;
                    Http.open("GET", url);
                    Http.send();
                }
                onReleased: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + stopRequest;
                    Http.open("GET", url);
                    Http.send();
                }
            }
            Button {
                Layout.preferredWidth: 120
                Rectangle {
                    width: parent.width/1.05
                    height: parent.height/1.1
                    radius: 5
                    color: "red"
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: "STOP"
                    }
                }
                onPressed: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + stopRequest;
                    Http.open("GET", url);
                    Http.send();
                }
                onReleased: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + stopRequest;
                    Http.open("GET", url);
                    Http.send();
                }

            }
            Button {
                Layout.preferredWidth: 120
                property string request: "/right"
                Rectangle {
                    width: parent.width/1.05
                    height: parent.height/1.1
                    radius: 5
                    color: "red"
                    Label {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text: "RIGHT"
                    }
                }                    onPressed: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + request;
                    Http.open("GET", url);
                    Http.send();

                }
                onReleased: function() {
                    const Http = new XMLHttpRequest();
                    const url = ipAddress + stopRequest;
                    Http.open("GET", url);
                    Http.send();
                }
            }
        }
        Button {
            Layout.preferredWidth: 120
            property string request: "/backward"
            Rectangle {
                width: parent.width/1.05
                height: parent.height/1.1
                radius: 5
                color: "red"
                Label {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: "BACKWARD"
                }
            }            Layout.alignment: Qt.AlignHCenter
            onPressed: function() {
                const Http = new XMLHttpRequest();
                const url = ipAddress + request;
                Http.open("GET", url);
                Http.send();

                Http.onreadystatechange = (e) => {
                    output.text = Http.responseText;
                }
            }
            onReleased: function() {
                const Http = new XMLHttpRequest();
                const url = ipAddress + stopRequest;
                Http.open("GET", url);
                Http.send();

                Http.onreadystatechange = (e) => {
                    output.text = Http.responseText;
                }
            }
        }
    }
}
