import QtQuick 2.15
import QtQuick.Layouts
import QtQuick.Controls

Item {
    id: controlPanel
    implicitWidth: rl.implicitWidth
    implicitHeight: rl.implicitHeight
    property string ipAddress: "value"
    onIpAddressChanged: function() {
        motionControl.ipAddress = ipAddress;
        armControl.ipAddress = ipAddress;
    }

    ColumnLayout {
        implicitWidth: children.implicitWidth
        implicitHeight: children.implicitHeight
        id: rl
        spacing: 10
        Text {
            id: output
            text: "No Command"
        }
        RowLayout {
            MotionControl {
                id: motionControl
                height: children.implicitHeight
                width: children.implicitWidth
            }
            ArmControl {
                id: armControl
                height: children.implicitHeight
                width: children.implicitWidth
            }
        }
    }
}
