import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts
import QtQuick.Controls

Window {
    id: root
    width: 900
    height: 600
    visible: true
    title: qsTr("JETBotControls")

    property string ipAddress: ""
    onIpAddressChanged: function () {
        liveCam.ipAddress = ipAddress;
        controlPanel.ipAddress = ipAddress;
    }

    ColumnLayout {
        spacing: 20
        width: parent.width
        height: implicitHeight
        Logo {
            id: logo
            Layout.fillWidth: true
            height: implicitHeight
            width: implicitWidth
        }

        IPInputBox {
            id: inputBox
            Layout.fillWidth: true
            width: implicitWidth
            height: implicitHeight
            onSendNewIp: function (ip) {
                root.ipAddress = ip;
            }
        }
        LiveCam {
            id: liveCam
            Layout.alignment: Qt.AlignHCenter
            height: this.height
            width: this.width
            ipAddress: root.ipAddress
        }
        Control {
            padding: 10
            Layout.alignment: Qt.AlignCenter
            contentItem: ControlPanel {
                id: controlPanel
                height: implicitHeight
                width: implicitWidth
                Layout.alignment: Qt.AlignBottom
                ipAddress: root.ipAddress
            }
        }
    }
}
