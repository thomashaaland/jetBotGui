import QtQuick
import QtMultimedia
import QtWebEngine

/**
  QML for running a cameraview mounted and served on
  a Jetbot. Requires an IP address and runnning server
  on the Jetbot.
  **/
Item {
    property string ipAddress: ""
    onIpAddressChanged: {
        webview.reload();
    }

    property string request: "/camera"
    width: 300
    height: 200

    Rectangle {
        anchors.fill: parent
        color: "black"
        WebEngineView {
            id: webview
            anchors.fill: parent
            url: ipAddress + request
        }
    }
}
