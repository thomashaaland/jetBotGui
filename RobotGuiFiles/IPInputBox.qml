import QtQuick 2.15
import QtQuick.Layouts
import QtQuick.Controls

Item {
    implicitWidth: rl.implicitWidth
    implicitHeight: rl.implicitHeight
    signal sendNewIp(string ip)
    property string ipAddress: ""
    Rectangle {
        width: parent.width + 10
        height: parent.height + 10
        color: "#DDDDDD"
        Control {
            padding: 5
            contentItem: RowLayout {
                id: rl
                implicitWidth: children.width
                implicitHeight: children.height
                spacing: 0
                Text {
                    text: "Enter IP address: "
                }
                Rectangle {
                    color: "white"
                    width: 23
                    height: 18
                    TextInput {
                        id: ip1
                        anchors.fill: parent
                        text: "172"
                        cursorVisible: false
                        validator: IntValidator {bottom: 0; top: 255}
                    }
                }
                Text {
                    text: "."
                }
                Rectangle {
                    color: "white"
                    width: 23
                    height: 18
                    TextInput {
                        id: ip2
                        anchors.fill: parent
                        text: "27"
                        cursorVisible: false
                        validator: IntValidator {bottom: 0; top: 255}
                    }
                }
                Text {
                    text: "."
                }
                Rectangle {
                    color: "white"
                    width: 23
                    height: 18
                    TextInput {
                        id: ip3
                        anchors.fill: parent
                        text: "101"
                        cursorVisible: false
                        validator: IntValidator {bottom: 0; top: 255}
                    }
                }
                Text {
                    text: "."
                }
                Rectangle {
                    color: "white"
                    width: 23
                    height: 18
                    TextInput {
                        id: ip4
                        anchors.fill: parent
                        text: "185"
                        cursorVisible: false
                        validator: IntValidator {bottom: 0; top: 255}
                    }
                }
                Item {
                    Layout.leftMargin: 10
                }
                Button {
                    Layout.preferredWidth: 120
                    Rectangle {
                        id: physbutton
                        width: parent.width/1.05
                        height: parent.height/1.1
                        radius: 5
                        color: "red"
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: "CONNECT"
                        }
                    }
                    onClicked: function() {
                        const initRequest = "/init";
                        const url = "http://" + ip1.text + "." + ip2.text + "." + ip3.text + "." + ip4.text + ":5000";
                        const Http = new XMLHttpRequest();
                        Http.open("GET", url + initRequest);
                        Http.send();
                        Http.onreadystatechange = function() {
                            ipAddress = url;
                            sendNewIp(url);
                        }
                    }
                }
                Button {
                    Layout.preferredWidth: 120
                    Rectangle {
                        width: parent.width/1.05
                        height: parent.height/1.1
                        radius: 5
                        color: "red"
                        Label {
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            text: "DISCONNECT"
                        }
                    }
                    onClicked: function() {
                        const stopRequest = "/off";
                        const Http = new XMLHttpRequest();
                        Http.open("GET", ipAddress + stopRequest);
                        Http.send();
                        const url = "";
                        sendNewIp(url);
                    }
                }
            }
        }
    }
}
