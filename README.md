# Jetbot GUI
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
This is a project where we use the Jetbot and build an QML application to view the videofeed as well as controlling all possible movements of the robot. The project relies on Qt Quick and Flask to run.

![jetBitGui](jetBotGui.png)
## Requirments
* [QT](https://www.qt.io/)
* [MSVC](https://visualstudio.microsoft.com/downloads/)
* [Flask](https://flask.palletsprojects.com/en/2.2.x/)
* [openCV](https://opencv.org/)

Installing the program:
```
Import flaskeserver.py and robotcontrols.py to a folder in your jetbot robot environment. Then you have to run the flask server by using the command: python3 flaskserver.py
Build and Run the GUI application with QT
```

## Usage

```
Using the GUI application you have to connect to the robot by inputting the correct IP and clicking on Connect. If connected correctly the robot will move its arm slightly and you can see the videofeed in the application. Now you can control the robot using the buttons on screen.

```



### Contributing

## Thomas Haaland
GUI - QT programming with QML programming language and QQuick library

## Paal Marius Bruun Haugen
Flask Server - Jetson programming with python programming language and flask library

## License

