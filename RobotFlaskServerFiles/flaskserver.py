import os
import robotcontrol as rob
from flask import Flask, Response
from flask import request
import cv2 as cv
import time
from jetbot import Camera, bgr8_to_jpeg
import vision as v

camera = None

app = Flask(__name__)

#Setting up the camera
@app.route("/init")
def init():
    global camera 
    global vision
    camera = Camera.instance(width=300, height=300)
    return "init"

#######Moving functions that can be called by sending a request to the route url######
@app.route("/forward")
def forward():
    rob.step_forward()
    return "forward"

@app.route("/backward")
def backward():
    rob.step_backward()
    return "backward"

@app.route("/stop")
def stop():
    rob.stop()
    return "stopping"

@app.route("/left")
def left():
    rob.step_left()
    return "left"

@app.route("/right")
def right():
    rob.step_right()
    return "right"

@app.route("/xIn")
def xIn():
    rob.xIn()
    return "xIn"
@app.route("/yIn")
def yIn():
    rob.yIn()
    return "yIn"
    
@app.route("/xDe")
def xDe():
    rob.xDe()
    return "xDe"

@app.route("/yDe")
def yDe():
    rob.yDe()
    return "yDe"

@app.route("/clawopen")
def clawopen():
    rob.loose()
    return "loose"

@app.route("/clawclose")
def clawclose():
    rob.grab()
    return "grab"

@app.route("/armleft")
def panLeft():
    rob.ptLeft()
    return "ptLeft"

@app.route("/armright")
def panRight():
    rob.ptRight()
    return "ptRight"

@app.route("/camup")
def camUp():
    rob.cameraUp()
    return "campUp"

@app.route("/camdown")
def camDown():
    rob.cameraDown()
    return "camDown"
###################################################################################

#Camera stream function
@app.route("/camera")
def camera():
    return Response(gen(), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route("/stopcam")
def stopcamera():
    camera.stop()
    return "stopping cam"

#Generating new frames
def gen():
    while True:
        time.sleep(0.2)
        frame = camera.value
        frameOut = bgr8_to_jpeg(frame)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frameOut + b'\r\n\r\n')

@app.route("/off")
def off():
    rob.off()
    camera.stop()
    camera = None # Release the camera
    return "OFF"
    
#Starting the app
if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
#resetting the robot after application has been exited with CTRL+C
if camera is not None:
    camera.stop()
    camera = None # Release the camera
