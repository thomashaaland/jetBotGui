from SCSCtrl import TTLServo
from jetbot import Robot
import time
robot = Robot()
xPos = 100
yPos = 0
servoPos_4 = 0
servoPos_1 = 0  # Pan position of the camera.
servoPos_5 = 0  # The vertical position of the camera.
TTLServo.servoAngleCtrl(1, 0, 1, 150)
TTLServo.servoAngleCtrl(2, 0, 1, 150)
TTLServo.servoAngleCtrl(3, 0, 1, 150)
TTLServo.servoAngleCtrl(4, 0, 1, 150)
TTLServo.servoAngleCtrl(5, 0, 1, 150)

def limitCtl(maxInput, minInput, rawInput):
    if rawInput > maxInput:
        limitBuffer = maxInput
    elif rawInput < minInput:
        limitBuffer = minInput
    else:
        limitBuffer = rawInput
        
    return limitBuffer

def init():
    pass

    
def cameraUp():
    global servoPos_5  # Global variables declaration
    servoPos_5 = limitCtl(25, -40, servoPos_5-15)
    TTLServo.servoAngleCtrl(5, servoPos_5, 1, 150)

def cameraDown():
    global servoPos_5  # Global variables declaration
    servoPos_5 = limitCtl(25, -40, servoPos_5+15)
    TTLServo.servoAngleCtrl(5, servoPos_5, 1, 150)

# Camera turn right
def ptRight():
    global servoPos_1
    servoPos_1 = limitCtl(80, -80, servoPos_1+15)
    TTLServo.servoAngleCtrl(1, servoPos_1, 1, 150)

# Camera turn left
def ptLeft():
    global servoPos_1
    servoPos_1 = limitCtl(80, -80, servoPos_1-15)
    TTLServo.servoAngleCtrl(1, servoPos_1, 1, 150)
    
def stop():
    robot.stop()
    
def step_forward():
    robot.forward(0.9)
#     time.sleep(0.5)
#     robot.stop()

def step_backward():
    robot.backward(0.9)
#     time.sleep(0.5)
#     robot.stop()

def step_left():
    robot.left(0.9)
    #time.sleep(0.5)
    #robot.stop()

def step_right():
    robot.right(0.9)
    #time.sleep(0.5)
    #robot.stop()

def xIn():
    global xPos
    xPos += 15
    TTLServo.xyInput(xPos, yPos)

def xDe():
    global xPos
    xPos -= 15
    if xPos < 85:
        xPos = 85
    TTLServo.xyInput(xPos, yPos)

def yIn():
    global yPos
    yPos += 15
    TTLServo.xyInput(xPos, yPos)

def yDe():
    global yPos
    yPos -= 15
    TTLServo.xyInput(xPos, yPos)

def grab():
    global servoPos_4
    servoPos_4 -= 15
    print(servoPos_4)
    if servoPos_4 < -90:
        servoPos_4 = -90
    TTLServo.servoAngleCtrl(4, servoPos_4, 1, 150)
    
    
def loose():
    global servoPos_4
    servoPos_4+= 15
    if servoPos_4 > -10:
        servoPos_4 = -10
    TTLServo.servoAngleCtrl(4, servoPos_4, 1, 150)
    
def off():
    TTLServo.servoAngleCtrl(1, 0, 1, 150)
    TTLServo.servoAngleCtrl(2, 0, 1, 150)
    TTLServo.servoAngleCtrl(3, 0, 1, 150)
    TTLServo.servoAngleCtrl(4, 0, 1, 150)
    TTLServo.servoAngleCtrl(5, 0, 1, 150)
    
